// import SpeedRate from './SpeedRate.js';
import { setSpeedRate as setGameSpeedRate } from './SpeedRate.js';
import Card from './Card.js';
import Game from './Game.js';

// Отвечает является ли карта уткой.
function isDuck(card) {
  return card && card.quacks && card.swims;
}

// Отвечает является ли карта собакой.
function isDog(card) {
  return card instanceof Dog;
}

// Дает описание существа по схожести с утками и собаками
function getCreatureDescription(card) {
  if (isDuck(card) && isDog(card)) {
    return 'Утка-Собака';
  }
  if (isDuck(card)) {
    return 'Утка';
  }
  if (isDog(card)) {
    return 'Собака';
  }
  return 'Существо';
}
class Creature extends Card {
  getDescriptions() {
    return new Array(getCreatureDescription(this), super.getDescriptions());
  }
}
class Duck extends Creature {
  constructor() {
    super('Мирная утка', 2);
  }
  quacks() {
    console.log('quack');
  }
  swims() {
    console.log('float: both;');
  }
}
class Dog extends Creature {
  constructor(name = 'Пес-бандит', power = 3) {
    super(name, power);
  }
  swims() {
    console.log('float: none;');
  }
}
class Lad extends Dog {
  constructor() {
    super('Браток', 2);
    this.inGameCount++;
  }
  static inGameCount = 0;
  doAfterComingIntoPlay(gameContext, continuation) {
    const { currentPlayer, oppositePlayer, position, updateView } = gameContext;
    continuation();
    Lad.inGameCount++;
  }
  doBeforeRemoving(continuation) {
    //const { currentPlayer, oppositePlayer, position, updateView } = gameContext;
    continuation();
    Lad.inGameCount--;
  }
  static getBonus() {
    return (this.inGameCount * (this.inGameCount + 1)) / 2;
  }
  modifyDealedDamageToCreature(value, toCard, gameContext, continuation) {
    console.log(Lad.getBonus());
    continuation(Lad.getBonus());
    // Lad.getBonus();
  }
  modifyTakenDamage(value, fromCard, gameContext, continuation) {
    console.log(Lad.getBonus());
    continuation(Lad.getBonus());
  }
}

// Основа для утки.
// function Duck() {
//   this.quacks = () => {
//     console.log('quack');
//   };
//   this.swims = () => {
//     console.log('float: both;');
//   };
// }

// // Основа для собаки.
// function Dog() {
//   this.swims = () => {
//     console.log('float: none;');
//   };
// }

// Колода Шерифа, нижнего игрока.
const seriffStartDeck = [
  new Duck(),
  new Duck(),
  new Duck(),
  //   new Card('Мирный житель', 2),
  //   new Card('Мирный житель', 2),
];

// Колода Бандита, верхнего игрока.
const banditStartDeck = [new Lad(), new Lad()];
console.log(isDuck(seriffStartDeck[0]));
// Создание игры.
const game = new Game(seriffStartDeck, banditStartDeck);

// Глобальный объект, позволяющий управлять скоростью всех анимаций.
setGameSpeedRate(1);

// Запуск игры.
game.play(false, (winner) => {
  alert('Победил ' + winner.name);
});
